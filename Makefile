ver=$(shell date +%Y.%m.%d)

# mkarchiso flags
LABEL="Parabola"
PUBLISHER="Parabola Project"
APPLICATION="Parabola Live Medium"
PACCONFIG="overlay/etc/pacman.conf"

WORKDIR=work
INSTALL_DIR=libre
COMPRESS=xz

ARCH?=$(shell uname -m)

PWD=$(shell pwd)
NETname=$(PWD)/parabola-$(ver)-netinstall-$(ARCH).iso
COREname=$(PWD)/parabola-$(ver)-core-$(ARCH).iso

PACKAGES="$(shell cat packages.$(ARCH))"

kver_FILE=$(WORKDIR)/root-image/etc/mkinitcpio.d/kernel26.kver

all: net-iso core-iso torrent

# Rules for each type of image
core-iso: $(COREname)
net-iso: $(NETname)

$(COREname): core-pkgs base-fs
	mkarchiso -v -C $(PACCONFIG) -L $(LABEL) -P $(PUBLISHER) -A $(APPLICATION) -D $(INSTALL_DIR) -c $(COMPRESS) iso $(WORKDIR) $@
$(NETname): base-fs
	mkarchiso -v -C $(PACCONFIG) -L $(LABEL) -P $(PUBLISHER) -A $(APPLICATION) -D $(INSTALL_DIR) -c $(COMPRESS) iso $(WORKDIR) $@

# This is the main rule for make the working filesystem.
base-fs: root-image bootfiles initcpio overlay purge iso-mounts

# Rules for make the root-image for base filesystem.
root-image: $(WORKDIR)/root-image/.arch-chroot
$(WORKDIR)/root-image/.arch-chroot:
	mkarchiso -v -C $(PACCONFIG) -L $(LABEL) -P $(PUBLISHER) -A $(APPLICATION) -D $(INSTALL_DIR) -p base create $(WORKDIR)
	mkarchiso -v -C $(PACCONFIG) -L $(LABEL) -P $(PUBLISHER) -A $(APPLICATION) -D $(INSTALL_DIR) -p $(PACKAGES) create $(WORKDIR)

# Rule for make /boot
bootfiles: root-image
	mkdir -p $(WORKDIR)/iso/$(INSTALL_DIR)/boot/$(ARCH)
	cp $(WORKDIR)/root-image/boot/System.map26 $(WORKDIR)/iso/$(INSTALL_DIR)/boot/$(ARCH)/
	cp $(WORKDIR)/root-image/boot/vmlinuz26 $(WORKDIR)/iso/$(INSTALL_DIR)/boot/$(ARCH)/
	cp $(WORKDIR)/root-image/boot/memtest86+/memtest.bin $(WORKDIR)/iso/$(INSTALL_DIR)/boot/memtest
	cp $(WORKDIR)/root-image/usr/share/licenses/common/GPL2/license.txt $(WORKDIR)/iso/$(INSTALL_DIR)/boot/memtest.COPYING
	cp boot-files/splash.png $(WORKDIR)/iso/$(INSTALL_DIR)/boot/
	mkdir -p $(WORKDIR)/iso/syslinux
	cp $(WORKDIR)/root-image/usr/lib/syslinux/*.c32 $(WORKDIR)/iso/syslinux/
	cp $(WORKDIR)/root-image/usr/lib/syslinux/poweroff.com $(WORKDIR)/iso/syslinux/
	cp $(WORKDIR)/root-image/usr/lib/syslinux/isolinux.bin $(WORKDIR)/iso/syslinux/
	cp $(WORKDIR)/root-image/usr/lib/syslinux/memdisk $(WORKDIR)/iso/syslinux/
	cp $(WORKDIR)/root-image/usr/lib/syslinux/pxelinux.0 $(WORKDIR)/iso/syslinux/
	cp $(WORKDIR)/root-image/usr/lib/syslinux/gpxelinux.0 $(WORKDIR)/iso/syslinux/
	cp boot-files/syslinux/syslinux.cfg $(WORKDIR)/iso/syslinux/syslinux.cfg
	# Add pci.ids and modules.alias for hdt
	mkdir -p $(WORKDIR)/iso/syslinux/hdt/
	wget -O - http://pciids.sourceforge.net/v2.2/pci.ids | gzip -9 > $(WORKDIR)/iso/syslinux/hdt/pciids.gz
	cat $(WORKDIR)/root-image/lib/modules/$(shell grep ^ALL_kver $(kver_FILE) | cut -d= -f2)/modules.alias | gzip -9 > $(WORKDIR)/iso/syslinux/hdt/modalias.gz

# Rules for initcpio images
initcpio: $(WORKDIR)/iso/$(INSTALL_DIR)/boot/$(ARCH)/archiso.img
$(WORKDIR)/iso/$(INSTALL_DIR)/boot/$(ARCH)/archiso.img: mkinitcpio.conf $(WORKDIR)/root-image/.arch-chroot
	mkdir -p $(WORKDIR)/iso/$(INSTALL_DIR)/boot/$(ARCH)/
	mkinitcpio -c ./mkinitcpio.conf -b $(WORKDIR)/root-image -k $(shell grep ^ALL_kver $(kver_FILE) | cut -d= -f2) -g $@


# overlay filesystem
overlay:
	mkdir -p $(WORKDIR)/overlay/etc/pacman.d
	cp -r overlay $(WORKDIR)/

purge:
	cp overlay/etc/locale.nopurge $(WORKDIR)/root-image/etc/
	chroot $(WORKDIR)/root-image localepurge

# Rule to process isomounts file.
iso-mounts: $(WORKDIR)/iso/$(INSTALL_DIR)/isomounts
$(WORKDIR)/iso/$(INSTALL_DIR)/isomounts: isomounts root-image
	sed "s|@ARCH@|$(ARCH)|g" isomounts > $@


# Rule for make the [core] repo packages
core-pkgs:
	if [ -d core-pkgs ] ; then mv core-pkgs $(WORKDIR)/ ; fi
	./download-repo.sh core $(WORKDIR)/core-pkgs/src/core/pkg


# Clean-up all work
clean:
	if [ -d work/core-pkgs ] ; then mv work/core-pkgs . ; fi
	rm -rf $(WORKDIR) $(NETname) $(COREname) $(NETname).torrent $(COREname).torrent

torrent:
	./createtorrent $(NETname) $(COREname)


.PHONY: all core-iso net-iso
.PHONY: base-fs
.PHONY: root-image bootfiles initcpio overlay iso-mounts
.PHONY: core-pkgs
.PHONY: clean

